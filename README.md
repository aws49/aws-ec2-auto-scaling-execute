# EC2 Auto Scaling Execute

## About

**AWS EC2 Auto Scaling Group Execute**

**Script:** [`auto-scaling-group-exec.sh`](auto-scaling-group-exec.sh)  
**AWS CLI:** `version 2`  
**Language:** `Bash`  


This script for executing a command on all hosts of **AWS EC2 Auto Scaling Group** via **SSH**.

**`[!]` SSH does via Identity File (Private Key) without password.**


**Author:** `Oleksii Pavliuk` **(pavliuk.aleksey@gmail.com)**  
**Date:** `10/22/2020`


## Before using the script

Before using this script:
- make sure that the environment has **read-access** permissions to **AWS EC2** && **AWS EC2 Auto Scaling**;
- [install](https://docs.aws.amazon.com/cli/latest/userguide/cli-chap-install.html) and [configure](https://docs.aws.amazon.com/cli/latest/userguide/cli-chap-configure.html) **AWS CLI version 2**.


## Usage

```shell script
Usage: ./$auto-scaling-group-exec.sh [OPTIONS]
  OPTIONS:
     [required]:
        --auto-scaling-group-name ARG : a name of AWS Auto Scale Group
        --ssh-user                    : a SSH Username
        --ssh-identity-file           : a SSH Identity file (Private Key)
        --exec-cmd                    : a command which need to execute on hosts of
                                        auto scaling group via SSH
     [optional]:
        --aws-region ARG              : AWS Region [default: $AWS_REGION]
        --log-file-path ARG           : path to log file for logging [default: $LOG_FILE_PATH]
        -h | --help                   : show this usage

Example: ./auto-scaling-group-exec.sh --auto-scaling-group-name {GROUP_NAME} \
                                      --ssh-user {USERNAME} \
                                      --ssh-identity-file {PATH_TO_FILE} \
                                      --exec-cmd {CMD}
```
